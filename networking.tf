provider "aws" {
  region = "us-east-1"
}

resource "aws_vpc" "production-vpc" {
  cidr_block = var.cidr_blocks[0]

  tags = {
    Name = "main-one-vpc"
  }
}

variable "cidr_blocks"{
  description = "cidr block of vpc and subnet"
  type =list(string) 
}


resource "aws_subnet" "production-subnet" {
  vpc_id     = aws_vpc.production-vpc.id
  cidr_block = "10.0.1.0/24"

  tags = {
    Name = "Main-one-subnet"
  }
}
resource "aws_subnet" "production-RT" {
  vpc_id     = aws_vpc.production-vpc.id
  cidr_block = var.cidr_blocks[1]

  tags = {
    Name = "main-one-RT"
  }
}

output "production-vpc-id"{
  value = aws_vpc.production-vpc.id
}

output "production-subnet-id"{
  value = aws_subnet.production-subnet.id
}
